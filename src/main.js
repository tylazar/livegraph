// import 'babel-polyfill';
require('raw!../html/index.html');

import Chart from 'chart.js';
import ReconnectingWebSocket from 'ReconnectingWebSocket';
import Charts from './charts';
import ChartDispatcher from './chart_dispatcher';

window.onload = function() {

    var container = document.getElementById('canvas-container');
    var canvases = [];
    for (var i=0; i < chart_definitions.length; ++i){
        var canvas = document.createElement('canvas');
        container.appendChild(canvas);
        canvases.push(canvas);
    }
    chart_dispatcher = new ChartDispatcher(charts.makeChartJSCharts(canvases));
}

function handler(event) {
    var valid_json = true;
    var data;
    try {
        data = JSON.parse(event.data);
    } catch (err){
        valid_json = false;
    }
    if (valid_json == true){
        console.log(data);
        chart_dispatcher.dispatchData(data);
    }
}

var chart_definitions = [   //[   'CliffLeftSignal',
                            //    'CliffRightSignal'],
                            [   'prediction']
                        ]

var charts = new Charts(chart_definitions);

var chart_dispatcher;

//var ws = new WebSocket('ws://localhost:31337');
var ws = new ReconnectingWebSocket('ws://localhost:31337');//10.0.1.67:31337');
ws.onmessage = handler
