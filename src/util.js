/* Utility functions */

export function debug(...args) {
    console.log("[DEBUG]: ", ...args);
}

export function fitToContainer(elem) {
    elem.style.width = '100%';
    elem.style.height = '100%';
    elem.width = elem.offsetWidth;
    elem.height = elem.offsetHeight;
}

/** Create an array of the given shape, where an element at indices `(i,j,k,...)`
 *  is given by `func(i,j,k,...)` */
export function indexMap(func, ...shape) {
    /* Private function for performing an index map */
    function _indexMap(func, indices, ...shape) {
        indices = indices || [];
        if (shape.length > 0) {
            let ret = new Array(shape[0]);
            for (let i = 0; i < shape[0]; i++) {
                ret[i] = _indexMap(func, indices.concat(i), ...shape.slice(1));
            }
            return ret;
        } else {
            return func(...indices);
        }
    }
    return _indexMap(func, [], ...shape);
}