// import 'babel-polyfill';
require('raw!../html/index.html');


import Sparkline from './sparkline';
import Heatmap from './heatmap';

import {fitToContainer, indexMap} from './util';

var canvas = document.getElementById('canvas-1');
fitToContainer(canvas);
var canvas2 = document.getElementById('canvas-2');


// Define chart options & chart
var chartOptions = {
    ymax: 2,
    ymin: -2
};
var chart = new Sparkline(canvas, chartOptions);

// Generate some data
var data = _.map(_.range(15), (x) => Math.sin(3*x/Math.PI));

chart.update(data);

// Chart 2 /////////////////////////////////////////////////////////////
var chartOptions2 = {

}
var chart2 = new Heatmap(canvas2, chartOptions2);

var data2 = rand(3, 10);
chart2.update(data2);

// Debugging
function genData(n, m=1) {
    n = n || 1;
    let ret = new Array(n);
    return _.map(ret, x => _.random(-2, 2, true));
}

function rand(...shape) {
    return indexMap(() => _.random(-1, 1, true), ...shape);
}

// var intervalID = window.setInterval(x => chart.update(genData(2)), 500);
// var intervalID = window.setInterval(x => chart.update([Math.sin(Date.now()/1000)]), 15);

export {chart, canvas};