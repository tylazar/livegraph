import Chart from 'chart.js';

export default class Charts {

    constructor(chart_definitions){
        this.chart_definitions = chart_definitions;
    }

    makeChartJSCharts(canvases, step_size=1, max_timesteps=100){

        var charts = [];
        var chart_definitions = this.chart_definitions;

        for (var i=0; i < chart_definitions.length; ++i){

            // Initialize the datasets for a new chart
            var datasets = [];
            for (var j=0; j < chart_definitions[i].length; ++j){
                datasets.push( {    label: chart_definitions[i][j], 
                                    data: [] } );
            }

            // Define the new chart
            var chart = {   'current_timestep': -1,
                            'max_timesteps': max_timesteps, 
                            'datasets': datasets};

            chart['chart'] = new Chart(canvases[i], {
                type: 'scatter',
                data: {
                    datasets: chart['datasets']
                },
                options: {
                    scales: {
                        xAxes: [{
                            ticks: { stepSize: step_size }
                        }],
                        yAxes: [{
                            ticks: { beginAtZero: true }//, max: 1 }
                        }]
                    }
                }
            });

            charts.push(chart);
        }

        return charts;
    }

}