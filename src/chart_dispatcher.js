import Chart from 'chart.js';

export default class ChartDispatcher {

    constructor(charts, step_size=1){
        this.charts = charts;
        this.step_size = step_size;
    }

    dispatchData(data){

        var charts = this.charts;

        for (var i=0; i < charts.length; i++){

            var previous_timestep = charts[i]['current_timestep'];
            charts[i]['current_timestep'] = data['timestep'];

            // Add a new datapoint to each of the chart's datasets (if the data is available)
            for (var j=0; j < charts[i]['datasets'].length; ++j){
                if (charts[i]['datasets'][j]['label'] in data){
                    charts[i]['datasets'][j]['data'].push( {    x: charts[i]['current_timestep'], 
                                                                y: data[charts[i]['datasets'][j]['label']] } );
                }
            }

            // Only display max_timesteps datapoints at a time
            if (    previous_timestep != charts[i]['current_timestep'] &&
                    charts[i]['current_timestep'] > charts[i]['max_timesteps'] && 
                    (this.step_size == 1 || charts[i]['current_timestep'] % this.step_size == 1 )){

                for (var j=0; j < charts[i]['datasets'].length; ++j){
                    charts[i]['datasets'][j]['data'].splice(0, this.step_size);
                }

            }

            charts[i]['chart'].update(0);
        }
    }

}