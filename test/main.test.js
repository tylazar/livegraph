import 'babel-polyfill';
import assert from 'assert';


import * as main from '../src/main';

// Example generic test case
describe('Array', function(){
  describe('#indexOf()', function(){
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(4)); // 4 is not present in this array so indexOf returns -1
    })
  })
});

// Test case for function `foo` exported by src/main.js`
describe('main.foo', function() {
  it('should return the square of x', function() {
    assert.equal(1, main.foo(1));
    assert.equal(9, main.foo(3));
  });
});
