# livegraph

## Quick Start

Install development dependencies

```bash
npm install
```

Watch files and recompile on changes

```bash
npm watch
```

Run tests with Mocha.

```bash
npm test
```

Clean the `build` directory (remove all files).

```bash
npm clean
```

## Adapting Scripts to use Websockets

### Websocketd

Websocketd is essentially a bidirectional pipe (in the spirit of `socat`) that can be used to redirect `stdout` to a websocket and `stdin` to the script.
[It can be downloaded here](http://websocketd.com/)
This avoids having to write your own custom websocket code, although for handling sufficiently complicated things that might be necessary.

One approach might entail piping data directly to `websocketd`, but an alternative (for ease of reconnecting) might be to log readings to a file and then use `tail -f` so that an issue with `websocketd` doesn't take down the running script or vice-versa.

For example,

```bash
python data_generator.py > readings.txt

websocketd --port=31337 --staticdir=./build tail -f readings.txt
``` 